<?php include 'database.php'; ?>
<?php

//NOTE: Query to obtain the number of questions

$query = "SELECT COUNT(*) AS total FROM questions";


$result = $mysqli -> query($query) or die ($mysqli -> error.__LINE__);

$value = $result -> fetch_assoc();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>PHP Quizzer</title>
    <link rel="stylesheet" href="./bower_components/bootstrap/dist/css/bootstrap.min.css">
     <link rel="stylesheet" href="css/style.css">
     <link rel="stylesheet" href="css/material.min.css">
    
</head>

<body>

    <header>

        <div class="container">

            <h1>PHP Quizzer</h1>

        </div>

    </header>


    <main>

        <div class="container">

            <h2>Test your php kwnoledge </h2>
            <p>This is a multiple choice quiz to test your knowledge of PHP</p>

            <ul>

                <li><strong>Number of questions: </strong> <?php echo $value['total']; ?> </li>
                <li><strong>Type: </strong>Multple Choice</li>
                <li><strong>Estimated Time: </strong><?php echo $value['total'] * .5; ?> minutes</li>
            </ul>

            <a href="question.php?n=1" class="btn mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" >Start Quiz</a>

            <a href="add.php" id="color-btn" class="btn mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" >Add Questions</a>

        </div>

    </main>

    <footer>

        <div class="container">

            <p>Copyright &copy; 2016, PHP Quizzer</p>

        </div>

    </footer>

    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/material-design-lite/material.min.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>


</body>

</html>
