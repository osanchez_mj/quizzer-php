<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>PHP Quizzer</title>

    <link rel="stylesheet" href="./bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/material.min.css">

</head>

<body>


    <main>

        <div class="container">

              <h2>You are Done!!</h2>
              <p>Congrats you have already finish the test</p>
              <p>Final Score: <?php echo $_SESSION['score']; ?></p>
              <a href="question.php?n=1" class="btn mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"> Take Again </a>
       
       
        </div>

    </main>

    <footer>

        <div class="container">

            <p>Copyright &copy; 2016, PHP Quizzer</p>

        </div>

    </footer>

    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/material-design-lite/material.min.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>


</body>

</html>
<?php session_destroy(); ?>
