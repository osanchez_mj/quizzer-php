<?php include 'database.php'; ?>
<?php session_start(); ?>
<?php
	//Set question number
	$number = (int) $_GET['n'];

	/*
	*	Get total questions
	*/
	$query = "SELECT * FROM `questions`";
	//Get result
	$results = $mysqli->query($query) or die($mysqli->error.__LINE__);
	$total = $results->num_rows;

	/*
	*	Get Question
	*/
	$query = "SELECT * FROM `questions`
				WHERE question_number = $number";
	//Get result
	$result = $mysqli->query($query) or die($mysqli->error.__LINE__);

	$question = $result->fetch_assoc();

	/*
	*	Get Choices
	*/
	$query = "SELECT * FROM `choices`
				WHERE question_number = $number";
	//Get results
	$choices = $mysqli->query($query) or die($mysqli->error.__LINE__);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>PHP Quizzer</title>

    <link rel="stylesheet" href="./bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/material.min.css">

</head>

<body>

    <header>

        <div class="container">

            <h1>PHP Quizzer</h1>

        </div>

    </header>


    <main>

        <div class="container">

            <div class="current">
                Question <?php echo $question['question_number']; ?> of <?php echo $total; ?>
            </div>

            <p class="question"> 
            
                <?php
                
                    //IMP: Lo que esta en los corchetes se obtiene 'text' se obtienes de la base de datos de la tabla questions el campo text
                    echo $question['text'] ;
                ?> 
            
            </p>

            <form action="process.php" method="post">
                
                <?php while ($row = $choices ->fetch_assoc()):?>
                
                    <div class="separador">
                    <label  class="mdl-radio mdl-js-radio mdl-js-ripple-effect">
                        <input class="mdl-radio__button" name="choice" type="radio" value="<?php echo $row['id'];?>">
                        <span class="mdl-radio__label"><p><?php echo $row['text']?></p></span>
                    </label>

                </div>
                
                <?php endwhile;?>

                
                
                <button class="btn mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" type="submit"> Submit </button>
                
<!--NOTE: Este input es para pasar los valores mediante el metodo post en particular number para saber el numero de la pregunta-->
                <input type="hidden" name="number" value="<?php echo $number ?>">
                

            </form>


        </div>

    </main>

    <footer>

        <div class="container">

            <p>Copyright &copy; 2016, PHP Quizzer</p>

        </div>

    </footer>

    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/material-design-lite/material.min.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>


</body>

</html>
